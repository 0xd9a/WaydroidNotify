package application.waydroid.notify

import android.app.Activity
import android.content.ComponentName.unflattenFromString
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.core.widget.doOnTextChanged
import application.waydroid.notify.databinding.ActivityMainBinding

class MainActivity : Activity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toggle.setOnClickListener { startActivity(Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS)); }
        binding.host.setText(getSharedPreferences(packageName, Context.MODE_PRIVATE).getString("host", null) ?: getString(R.string.default_host))
        binding.host.doOnTextChanged { text, _, _, _ ->
            val host = text.let { if (it.isNullOrBlank()) getString(R.string.default_host) else it.toString() }
            getSharedPreferences(packageName, Context.MODE_PRIVATE).edit().putString("host", host).apply()
        }
    }

    override fun onResume() {
        super.onResume()
        binding.toggle.setImageResource(if (listenerEnabled()) R.drawable.ic_on else R.drawable.ic_off)
    }

    private fun listenerEnabled(): Boolean {
        val listeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners")
        if (!listeners.isNullOrBlank())
            for (listener in listeners.split(":"))
                if (unflattenFromString(listener)?.packageName == packageName)
                    return true
        return false
    }
}
