package application.waydroid.notify

import android.app.Notification
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.request.get
import io.ktor.http.Parameters
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class NotificationListener : NotificationListenerService() {
    private val httpClient = HttpClient(OkHttp)

    override fun onNotificationPosted(sbn: StatusBarNotification?) {
        super.onNotificationPosted(sbn)
        sbn?.notification?.extras?.let { bundle ->
            var title = sbn.packageName
            val content = bundle.getString(Notification.EXTRA_TITLE).toString()
            kotlin.runCatching {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) packageManager.getApplicationLabel(
                    packageManager.getApplicationInfo(
                        sbn.packageName, PackageManager.ApplicationInfoFlags.of(0L)
                    )
                ) else @Suppress("DEPRECATION") packageManager.getApplicationLabel(
                    packageManager.getApplicationInfo(
                        sbn.packageName, PackageManager.GET_META_DATA
                    )
                )
            }.onSuccess { title = it.toString() }.onFailure { it.printStackTrace() }
            sendToHost(title, content)
        }
    }

    private fun sendToHost(title: String, content: String) {
        val host = getSharedPreferences(packageName, Context.MODE_PRIVATE).getString("host", null) ?: getString(R.string.default_host)
        CoroutineScope(Dispatchers.IO).launch {
            kotlin.runCatching {
                val urlBuilder = URLBuilder(URLProtocol.HTTP, host, 5151, null, null, listOf(), Parameters.build {
                    append("title", title)
                    append("content", content)
                })
                httpClient.get(urlBuilder.build())
            }.onFailure { it.printStackTrace() }
        }
    }
}
