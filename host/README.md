### Install

```
sudo apt install python3-pip ruby-notify git
```

```
git clone https://codeberg.org/0xd9a/WaydroidNotify.git
```

```
pip install --upgrade pip Flask
```

```
python3 WaydroidNotify/host/run.py
```
