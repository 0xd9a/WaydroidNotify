from flask import Flask, request, Response
import subprocess

app = Flask(__name__)

@app.route("/")
def main():
	title = request.args.get('title', default = '', type = str)
	content = request.args.get('content', default = '', type = str)
	subprocess.call(["notify-send",title, content])
	return Response(status = 200)

if __name__ == "__main__":
    app.run(host="192.168.240.1", port=5151)
